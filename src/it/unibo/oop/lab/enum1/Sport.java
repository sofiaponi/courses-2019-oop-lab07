/**
 * 
 */
package it.unibo.oop.lab.enum1;

/**
 * Represents an enumeration or declaring sports;
 * 
 * 1) Complete the definition of the enumeration.
 * 
 */
public enum Sport {
	SOCCER, F1, MOTOGP, VOLLEY, BASKET, BIKE;

	
}
